<?php namespace Lareja\Web\Controllers;

use Flash;
use DateTime;
use Backend\Classes\Controller;
use BackendMenu;
use Lareja\Web\Constants;
use Lareja\Web\Models\Person;
use Lareja\Web\Models\State;
use Lareja\Web\Models\Place;
use Illuminate\Support\Facades\DB;
use Lareja\Web\Models\Reservation;
use Lareja\Web\Models\ReservationHost;
use Lareja\Web\Models\ReservationExtra;
use Lareja\Web\Models\ReservationPriceItem;
use Lareja\Web\Models\PriceItem;

class Reservations extends Controller
{

    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $recordId;
    public $data;

    public $requiredPermissions = [
        'reservationManager'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->addJs("/plugins/lareja/web/assets/scripts/reservation.js", "1.0.0");
        $this->addCss("/plugins/lareja/web/assets/styles/reservation.css", "1.0.0");
        BackendMenu::setContext('Lareja.Web', 'backend', 'reservations');
    }

	public function create($context = null){

		$this->getCreationFormData();
		return $this->asExtension('FormController')->create($context);

	}

    public function edit_prices($denomination=null, $price=null){
        if (!is_null($denomination) && !is_null($price)){
            PriceItem::where('denomination',$denomination)->update(['price'=>intval($price)]);
        }
        $data['prices'] = PriceItem::select('denomination','price')
                                        ->orderBy('price')
                                        ->get();
        return $this->makePartial('edit_prices',$data);
    }
    public function mp_notifications($id=null, $state=0){
    
        if (!is_null($id)){
            ReservationExtra::where('id',$id)->update(['multipurpose_checked'=>$state]);
        }
        define('MULTIPURPOSE_ID',4);
        $mp_notifs = ReservationExtra::select('lareja_web_reservation_extra.id',
            'date_from','date_to','people_number','details','multipurpose_checked',
            'lareja_web_person.name',
            'lareja_web_person.last_name')
			->join('lareja_web_person','lareja_web_reservation_extra.responsible_id','=','lareja_web_person.id')
            ->where('place_id',MULTIPURPOSE_ID)
            ->orderBy('id', 'desc')
            ->limit(50)
            ->get()->toarray();
        /*echo '<pre>'; var_dump($mp_notifs); echo '</pre>'; exit ;*/
        $data['notifications'] = $mp_notifs;
        return $this->makePartial('mp_notifications',$data);
    }

	public function create_onSave(){

		$data = post("data");
		$data['hosts'][0]['person_id'] = $data['person_id'];


		$workshop_count = 0;
		for($i=0;$i<count($data['hosts']);$i++){
			if ($data['hosts'][$i]['workshop']){
				$workshop_count += 1;
			}
			unset($data['hosts'][$i]['workshop']);
		}

		$reservation = array(
			'total_amount' 		=> count($data["hosts"]) * Constants::RESERVATION_NIGHT_PRICE,
			'workshop_people' 	=> 0,
			'state_id' 			=> Constants::RESERVATION_APPROVED,
			'responsible_id' 	=> $data["person_id"],
			'created_at' 		=> date('Y-m-d H:i:s'),
			'updated_at' 		=> date('Y-m-d H:i:s')
		);

		$id = Reservation::insertGetId($reservation);

		for($i=0;$i<count($data['hosts']);$i++){
			$data['hosts'][$i]['reservation_id'] = $id;
		}

		ReservationHost::insert($data['hosts']);

		Flash::success("Se guardó todo y no hubo ningún error");
    //todo: proper redirect
    //if ($redirect = $this->makeRedirect('create', $model)) {
    //      return $redirect;
    //  }
	}

    public function update($recordId, $context = null)
	{
		//
		// Se llama cuando cargo el formulario de actualización
		//
		$this->recordId = $recordId;
		$this->getReservationFormData();
		$this->addReservationAmounts();
		return $this->asExtension('FormController')->update($recordId, $context);
	}

    public function update_onSave($recordId, $context = null)
	{
        $model = $this->formFindModelObject($recordId);

		$data = post('data');

		Reservation::where('id','=',$recordId)
					->update($data['reservation']);

		ReservationHost::where('reservation_id','=',$recordId)->delete();

        if (isset($data['hosts'])){
            for( $i=0; $i<count($data['hosts']); $i++ ){
                $data['hosts'][$i]['reservation_id'] = $recordId;
                $data['hosts'][$i]['enabled']   = isset($data['hosts'][$i]['enabled'])  ? 1 : 0;
                $data['hosts'][$i]['linens']    = isset($data['hosts'][$i]['linens'])   ? 1 : 0;
                $data['hosts'][$i]['hosting']   = isset($data['hosts'][$i]['hosting'])  ? 1 : 0;
            }
            ReservationHost::insert($data['hosts']);
        }

		Flash::success("Reserva actualizada con éxito");

        if ($redirect = $this->makeRedirect('update', $model)) {
            return $redirect;
        }
	}

	public function remap(){}

	public function getReservationFormData(){

        $months = [
            'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ];

		// consultas SQL

		$reservation_data = Reservation::select('lareja_web_reservation.is_keeper',
			'lareja_web_reservation.total_amount',
			'lareja_web_reservation.paid_amount',
			'lareja_web_reservation.created_at',
			'lareja_web_reservation.workshop_people',
			'lareja_web_reservation.responsible_category',
			'lareja_web_reservation.responsible_category_2',
			'lareja_web_reservation.responsible_category_3',
			'lareja_web_person.name as responsible_name',
			'lareja_web_person.last_name as responsible_last_name',
			'lareja_web_person.id as responsible_id',
			'lareja_web_person.email as responsible_email',
			'lareja_web_person.phone as responsible_phone',
			'lareja_web_person.reservation_comments as comments',
			'lareja_web_state.id as state_id')
			->join('lareja_web_person','lareja_web_reservation.responsible_id','=','lareja_web_person.id')
			->join('lareja_web_state','lareja_web_reservation.state_id','=','lareja_web_state.id')
			->where('lareja_web_reservation.id','=',$this->recordId)
			->first()->toarray();

		$reservation_hosts = ReservationHost::select('person_id','from','to','place_id','enabled','hosting','linens',
			'lareja_web_person.name',
			'lareja_web_person.last_name')
			->join('lareja_web_person','lareja_web_reservation_host.person_id','=','lareja_web_person.id')
			->where('reservation_id',$this->recordId)
			->get();

		$reservation_workshop = ReservationExtra::select( 'date_from', 'date_to', 'people_number', 'details', 'oven')
			->where('reservation_id','=',$this->recordId)
			->get();

		$states_data = State::select('id','name')->where('id','>',1)->get();
		$places_data = Place::select('id','name')->where('capacity','>',0)->get();

		// Fin consultas SQL

		$this->data = $reservation_data;
		$this->data['created_at'] = (new DateTime($reservation_data['created_at']))->format('d/m/Y H:i');

		$this->data['states'] 	= array();
		$this->data['places'] 	= array();
		$this->data['hosts'] 	= array();

		foreach($states_data as $state){
			$this->data['states'][] = $state;
		}

		foreach($reservation_hosts as $host){
			$this->data['hosts'][] = $host;
		}

		foreach($places_data as $place){
			$this->data['places'][] = $place;
		}

        if (isset($reservation_workshop[0])){
            $date_parts = explode('-',explode(' ',$reservation_workshop[0]['date_from'])[0]);
            $from_year  = $date_parts[0];
            $from_month = $date_parts[1];
            $from_day   = $date_parts[2];

            $date_parts = explode('-',explode(' ',$reservation_workshop[0]['date_to'])[0]);
            $to_year    = $date_parts[0];
            $to_month   = $date_parts[1];
            $to_day     = $date_parts[2];

            if ($from_year == $to_year){
                if ($from_month == $to_month){
                    $dates = 'Del ' . $from_day . ' al ' . $to_day . ' de ' . $months[((int)$from_month)-1];
                }
                else{
                    $dates = 'Del ' . $from_day . ' de ' . $months[((int)$from_month)-1] . ' al ' . $to_day . ' de ' . $months[((int)$to_month)-1];
                }
                $dates .= ' de ' . $from_year;
            }
            else{
                $dates  = 'Del ' . $from_day;
                $dates .= ' de ' . $months[((int)$from_month)-1];
                $dates .= ' de ' . $from_year;
                $dates .= ' al ' . $to_day;
                $dates .= ' de ' . $months[((int)$to_month)-1];
                $dates .= ' de ' . $to_year;
            }

            $this->data['workshop'] = $reservation_workshop[0];
            $this->data['workshop_dates'] = $dates;
        }
        else{
            $this->data['workshop'] = null;
        }
	}

	public function getCreationFormData(){

		$this->data['persons'] 	= Person::select('id','name','last_name','email')->get()->toarray();
		$states_data 	= State::select('id','name')->get();
		$places_data 	= Place::select('id','name')->where('capacity','>',0)->get();

		$this->data['states'] 	= array();
		$this->data['places'] 	= array();


		foreach($states_data as $state){
			$this->data['states'][] = $state;
		}

		foreach($places_data as $place){
			$this->data['places'][] = $place;
		}
	}

    public function addReservationAmounts(){
        $hosts = ReservationHost::select(DB::raw('lareja_web_reservation_host.id, 
										concat(lareja_web_person.name," ",lareja_web_person.last_name) as name,
                                        if (lareja_web_reservation_host.hosting=1,lareja_web_reservation_price_item.price,0)  as night_price,
                                        abs(datediff(`from`,`to`)) as days ,
                                        if (lareja_web_reservation_host.hosting=1,
                                            abs(datediff(`from`,`to`)) * lareja_web_reservation_price_item.price, 0
                                        ) as total_price
                                    '))
							->from('lareja_web_reservation_host')
							->join('lareja_web_person','lareja_web_reservation_host.person_id','=','lareja_web_person.id')
							->join('lareja_web_reservation_price_item','lareja_web_reservation_host.reservation_id','=','lareja_web_reservation_price_item.reservation_id')
							->where('lareja_web_reservation_host.reservation_id',$this->recordId)
							->where('lareja_web_reservation_price_item.price_item_id',"host_night")
							->where('lareja_web_reservation_host.enabled',1)
                            ->get()->toarray();

        $linens = ReservationHost::select(DB::raw('lareja_web_reservation_host.id, 
										concat(lareja_web_person.name," ",lareja_web_person.last_name) as name,
                                        if (lareja_web_reservation_host.linens=1,lareja_web_reservation_price_item.price,0) as price,
                                        abs(datediff(`from`,`to`)) as days ,
                                        if (lareja_web_reservation_host.linens=1,
                                            abs(datediff(`from`,`to`)) * lareja_web_reservation_price_item.price, 0
                                        ) as total_price
                                    '))
							->from('lareja_web_reservation_host')
							->join('lareja_web_person','lareja_web_reservation_host.person_id','=','lareja_web_person.id')
							->join('lareja_web_reservation_price_item','lareja_web_reservation_host.reservation_id','=','lareja_web_reservation_price_item.reservation_id')
							->where('lareja_web_reservation_host.reservation_id',$this->recordId)
							->where('lareja_web_reservation_price_item.price_item_id',"linens")
							->where('lareja_web_reservation_host.enabled',1)
                            ->get()->toarray();

        for ($i = 0; $i < count($hosts); $i++){
            $hosts[$i]['linens_price'] = isset($linens[$i]['price']) ? $linens[$i]['price'] : 0;
            $hosts[$i]['total_price'] += isset($linens[$i]['price']) ? $linens[$i]['price'] * $hosts[$i]['days'] : 0;
        }

        $workshop_days = ReservationExtra::select(DB::raw('lareja_web_reservation_extra.id, 
                                                abs(datediff(`date_from`,`date_to`)) as days ,
                                                people_number as people_number ,
                                                lareja_web_reservation_price_item.price as person_price,
                                                lareja_web_reservation_price_item.price * people_number as price,
                                                if(abs(datediff(`date_from`,`date_to`)) = 0, 1, abs(datediff(`date_from`,`date_to`))) * people_number * lareja_web_reservation_price_item.price as total_price
                                    '))
							->from('lareja_web_reservation_extra')
							->join('lareja_web_reservation_price_item','lareja_web_reservation_extra.reservation_id','=','lareja_web_reservation_price_item.reservation_id')
							->where('lareja_web_reservation_extra.reservation_id',$this->recordId)
							->where('lareja_web_reservation_price_item.price_item_id',"workshop_use_day")
                            ->get()->toarray();

        if (isset($workshop_days[0]) && $workshop_days[0]['days'] == 0){
            $workshop_days[0]['days'] = 1;
        }
        $oven_use      = ReservationPriceItem::select('price as total_price')
                            ->where('price_item_id','oven_use')
                            ->where('reservation_id',$this->recordId)
                            ->get()->all();
        $total_price    = $this->sum_prices($hosts);
        $total_price   += $this->sum_prices($workshop_days);
        $total_price   += $this->sum_prices($oven_use);
		$this->data['hosts_prices']     = $hosts;
		$this->data['workshop_days']    = $workshop_days;
		$this->data['oven_use']         = $oven_use;
		$this->data['total_price']      = $total_price;
    }

    function sum_prices($input){
        $total_price = 0;
        foreach($input as $row){
            $total_price += $row['total_price'];
        }
        return $total_price;
    }
}

