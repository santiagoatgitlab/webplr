<?php namespace Lareja\Web\Models;

use Model;

/**
 * Model
 */
class EventSchedule extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /* Nada */
    public $belongsTo = [
        'event' => 'Lareja\Web\Models\Event'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lareja_web_event_schedule';
}