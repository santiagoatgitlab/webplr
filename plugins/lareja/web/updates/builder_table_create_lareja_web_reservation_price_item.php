<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLarejaWebReservationPriceItem extends Migration
{
    public function up()
    {
        Schema::create('lareja_web_reservation_price_item', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('reservation_id');
            $table->string('price_item_id');
            $table->integer('price');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lareja_web_reservation_price_item');
    }
}
