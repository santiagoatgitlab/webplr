<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLarejaWebEvent extends Migration
{
    public function up()
    {
        Schema::create('lareja_web_event', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 255);
            $table->binary('picture');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lareja_web_event');
    }
}
