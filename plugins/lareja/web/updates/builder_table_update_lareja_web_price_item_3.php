<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebPriceItem3 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_price_item', function($table)
        {
            $table->dropColumn('sum_up');
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_price_item', function($table)
        {
            $table->boolean('sum_up')->default(1);
        });
    }
}
