<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebActivity3 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_activity', function($table)
        {
            $table->string('link', 1023)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_activity', function($table)
        {
            $table->dropColumn('link');
        });
    }
}
