<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebReservationExtra extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_reservation_extra', function($table)
        {
            $table->dateTime('date_from')->nullable(false)->unsigned(false)->default(null)->change();
            $table->dateTime('date_to')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_reservation_extra', function($table)
        {
            $table->date('date_from')->nullable(false)->unsigned(false)->default(null)->change();
            $table->date('date_to')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
