<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebPerson4 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_person', function($table)
        {
            $table->boolean('from_reservation')->default(0);
            $table->text('reservation_comments')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_person', function($table)
        {
            $table->dropColumn('from_reservation');
            $table->dropColumn('reservation_comments');
        });
    }
}
