<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebEvent extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_event', function($table)
        {
            $table->string('date', 255);
            $table->text('description');
            $table->text('info');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->dropColumn('picture');
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_event', function($table)
        {
            $table->dropColumn('date');
            $table->dropColumn('description');
            $table->dropColumn('info');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->binary('picture');
        });
    }
}
