<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebReservationExtra3 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_reservation_extra', function($table)
        {
            $table->boolean('oven')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_reservation_extra', function($table)
        {
            $table->dropColumn('oven');
        });
    }
}
