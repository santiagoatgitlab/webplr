<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLarejaWebReservationExtra extends Migration
{
    public function up()
    {
        Schema::create('lareja_web_reservation_extra', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('place_id')->unsigned();
            $table->integer('reservation_id')->nullable()->unsigned();
            $table->date('date_from');
            $table->date('date_to');
            $table->integer('people_number')->unsigned();
            $table->text('details')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lareja_web_reservation_extra');
    }
}
