<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebEvent2 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_event', function($table)
        {
            $table->text('info')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_event', function($table)
        {
            $table->text('info')->nullable(false)->change();
        });
    }
}
