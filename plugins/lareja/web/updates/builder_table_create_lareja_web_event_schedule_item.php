<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLarejaWebEventScheduleItem extends Migration
{
    public function up()
    {
        Schema::create('lareja_web_event_schedule_item', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('event_schedule_id')->unsigned();
            $table->time('time');
            $table->string('title', 255);
            $table->string('owner', 255)->nullable();
            $table->text('platform')->nullable();
            $table->string('contact', 255)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lareja_web_event_schedule_item');
    }
}
