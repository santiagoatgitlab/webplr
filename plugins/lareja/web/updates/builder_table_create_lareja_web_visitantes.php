<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLarejaWebVisitantes extends Migration
{
    public function up()
    {
        Schema::create('lareja_web_visitantes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->date('fecha');
            $table->string('nombre', 255);
            $table->string('apellido', 255);
            $table->string('email', 255);
            $table->string('telefono', 255);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->index(['fecha', 'nombre', 'apellido']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lareja_web_visitantes');
    }
}
