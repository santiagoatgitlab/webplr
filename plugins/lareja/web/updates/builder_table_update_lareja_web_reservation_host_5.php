<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebReservationHost5 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_reservation_host', function($table)
        {
            $table->boolean('hosting')->default(1);
            $table->boolean('linens')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_reservation_host', function($table)
        {
            $table->dropColumn('hosting');
            $table->dropColumn('linens');
        });
    }
}
