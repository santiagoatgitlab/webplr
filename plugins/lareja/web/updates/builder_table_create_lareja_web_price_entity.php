<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLarejaWebPriceEntity extends Migration
{
    public function up()
    {
        Schema::create('lareja_web_price_entity', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('denomination');
            $table->integer('price');
            $table->primary(['denomination']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lareja_web_price_entity');
    }
}
