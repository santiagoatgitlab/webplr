<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebReservation13 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_reservation', function($table)
        {
            $table->string('responsible_category', 255)->nullable();
            $table->string('responsible_category_2', 255)->nullable();
            $table->string('responsible_category_3', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_reservation', function($table)
        {
            $table->dropColumn('responsible_category');
            $table->dropColumn('responsible_category_2');
            $table->dropColumn('responsible_category_3');
        });
    }
}
