<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebPriceItem2 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_price_item', function($table)
        {
            $table->boolean('sum_up')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_price_item', function($table)
        {
            $table->dropColumn('sum_up');
        });
    }
}
