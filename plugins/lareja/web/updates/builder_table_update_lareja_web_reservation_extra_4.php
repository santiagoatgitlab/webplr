<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebReservationExtra4 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_reservation_extra', function($table)
        {
            $table->string('workshop_categories', 255)->nullable();
            $table->boolean('multipurpose_checked')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_reservation_extra', function($table)
        {
            $table->dropColumn('workshop_categories');
            $table->dropColumn('multipurpose_checked');
        });
    }
}
