<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebReservationExtra2 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_reservation_extra', function($table)
        {
            $table->integer('responsible_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_reservation_extra', function($table)
        {
            $table->dropColumn('responsible_id');
        });
    }
}
