<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebPriceItem extends Migration
{
    public function up()
    {
        Schema::rename('lareja_web_price_entity', 'lareja_web_price_item');
    }
    
    public function down()
    {
        Schema::rename('lareja_web_price_item', 'lareja_web_price_entity');
    }
}
