<?php namespace Lareja\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLarejaWebEvent3 extends Migration
{
    public function up()
    {
        Schema::table('lareja_web_event', function($table)
        {
            $table->renameColumn('name', 'title');
        });
    }
    
    public function down()
    {
        Schema::table('lareja_web_event', function($table)
        {
            $table->renameColumn('title', 'name');
        });
    }
}
