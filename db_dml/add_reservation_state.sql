delete from lareja_web_state;
insert into lareja_web_state (id,name) values (1,'Pendiente de confirmación');
insert into lareja_web_state (id,name) values (2,'Pendiente de aprobación');
insert into lareja_web_state (id,name) values (3,'Aprobada');
insert into lareja_web_state (id,name) values (4,'Cancelada');
insert into lareja_web_state (id,name) values (5,'Finalizada');

