$(document).ready(function(){
    setInterval(function(){ 
        nextPicutre(); 
    }, 5000);
});

function nextPicutre(){
    console.log('next picture');
    if ($('.home-gallery img:visible').next().size() > 0){
        $next = $('.home-gallery img:visible').next();
    }
    else{
        $next = $('.home-gallery img:first');
    }
    $('.home-gallery img:visible').fadeOut(
        450, 
        function (){
            if ($next != null){
                $next.fadeIn(450);
            }
        }
    );
}
