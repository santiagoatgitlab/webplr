$(document).ready(function(){
	if ($('#centros_check').prop('checked')){
		$('.centros .hosts').css('display','block');
        $('.centros .hosts input, .centros .hosts select').attr('required','required');
        $('.centros .hosts .email').removeAttr('required');
	}
	initElements();
	initNamesAndCalendars();
    $show_android_chrome_advice = $('#is_android_chrome').val() == 1;
});

function initElements(){
	$('#add_host').click(function(){
		addHostLine();
	});
	$('.hosts .host button.remove').click(function(){
		$(this).closest('.host').remove();
	});
	$('#taller_check').change(function(){
		checkWorkshop();
	});
	$('#centros_check').change(function(){
		checkCenters();
	});
    $('#workshop_glass').click(function(){
        if ($(this).prop('checked')){
            $('#workshop_oven_container').show();
        }
        else{
            $('#workshop_oven_container').hide();
        }
    });
    $('form').submit(function(){
        if ($('#taller_check:checked, #centros_check:checked').size() > 0){
            if ($show_android_chrome_advice){
                showAndroidChromeAdvice();
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    });
    $('#workshop_from').datepicker({ dateFormat: 'dd-mm-yy', minDate: 0 })
    $('#workshop_to').datepicker({ dateFormat: 'dd-mm-yy', minDate: 0 })
	checkWorkshop();
    checkCenters();
}

function showAndroidChromeAdvice(){
    $('#android_chrome_advice').css('display','flex');
    $('#android_chrome_advice .ok_button').click(function(){
        $show_android_chrome_advice = false;
        $('form').submit();
    });
}

function checkWorkshop(){
	if ($('#taller_check').prop('checked')){
		$('.taller_cantidad').css('display','block');
        set_workshop_names();
	}
	else{
		$('.taller_cantidad').css('display','none');
		$('.workshop_input').removeAttr('name').removeAttr('required');
	}
}

function checkCenters(){
	if ($('#centros_check').prop('checked')){
		$('.centros .centros_input').css('display','block');
        $('.centros .hosts input, .centros .hosts select').attr('required','required');
        $('.centros .hosts .email').removeAttr('required');
	}
	else{
		$('.centros .centros_input').css('display','none');
        $('.centros .hosts input, .centros .hosts select').removeAttr('required');
	}
}

function addHostLine(){
	$last_place = $('.hosts .host:last .place').val();
	removeDatepickers($('.hosts .host:last'));
	$('.hosts .host:last').clone().appendTo('.hosts');
	$('.hosts .host:last .place').val($last_place);
	$('.hosts .host:last .name').val('');
	$('.hosts .host:last .last_name').val('');
	$('.hosts .host:last .email').val('');
	$('.hosts .host:last button.remove').show();
	$('.hosts .host:last button.remove').click(function(){
		$(this).closest('.host').remove();
        onHostsNumberChanged();
	});
	initNamesAndCalendars();
	$('.hosts .host:last .name').focus();
    onHostsNumberChanged();
}

function removeDatepickers(element){
	if (element.find('.date_from').hasClass("hasDatepicker")){
		element.find('.date_from').datepicker( "destroy" );
		element.find('.date_from').removeClass("hasDatepicker").removeAttr('id');
	}
	if (element.find('.date_to').hasClass("hasDatepicker")){
		element.find('.date_to').datepicker( "destroy" );
		element.find('.date_to').removeClass("hasDatepicker").removeAttr('id');
	}
}

function initNamesAndCalendars(){
	curElementPos = 1;
	$('.hosts .host').each(function(){

		removeDatepickers($(this));	
		$(this).find('.name').attr('name', 'hosts[' + curElementPos + '][name]');
		$(this).find('.last_name').attr('name', 'hosts[' + curElementPos + '][last_name]');
		$(this).find('.email').attr('name', 'hosts[' + curElementPos + '][email]');
		$(this).find('.date_from').attr('name', 'hosts[' + curElementPos + '][date_from]');
		$(this).find('.date_to').attr('name', 'hosts[' + curElementPos + '][date_to]');
		$(this).find('.place').attr('name', 'hosts[' + curElementPos + '][place]');
		$(this).find('.date_from').attr('id', 'from_' + curElementPos);
		$(this).find('.date_to').attr('id', 'to_' + curElementPos);
		$('#from_' + curElementPos).datepicker({ dateFormat: 'dd-mm-yy', minDate: 0 });
		$('#to_' + curElementPos).datepicker({ dateFormat: 'dd-mm-yy', minDate: 0 });
		curElementPos++;

	});
}

function set_workshop_names(){
    $('#workshop_from').attr('name','workshop_from').attr('required','required');
    $('#workshop_to').attr('name','workshop_to').attr('required','required');
    $('#workshop_people').attr('name','workshop_people').attr('required','required');
    $('#workshop_comments').attr('name','workshop_comments');

    $('#workshop_ceramic').attr('name','workshop_categories[ceramic]');
    $('#workshop_metals').attr('name','workshop_categories[metals]');
    $('#workshop_perfume').attr('name','workshop_categories[perfume]');
    $('#workshop_fire').attr('name','workshop_categories[fire]');
    $('#workshop_cold').attr('name','workshop_categories[cold]');
    $('#workshop_glass').attr('name','workshop_categories[glass]');
    $('#workshop_oven').attr('name','workshop_oven');
}               

function onHostsNumberChanged(){
    if ( $('.hosts .host').length == 6 ){
        $('#add_host').attr('disabled','disabled');
    }
    else{
        $('#add_host').removeAttr('disabled');
    }
}
