$(document).ready(function(){
	$('.calendars .day.occupied').click(function(event){
		openModal($(this));
	});
	$('.details-modal .close-button').click(function(){
		$(this).parent().parent().hide();
	});
    select_change($('select[name=place]').val());
    init_radios();
});
function openModal(target){
    $daystamp   = target.data('daystamp');
    $daydetails = $('.day-detail[data-daystamp='+$daystamp+']').clone();
	$('.details-modal').css('display','flex');
	$('.details-modal .container .content')[0].innerHTML = $daydetails[0].innerHTML; //Using plain javascript here because .html() jquery method was appending instead of replacing!
}
function init_radios(){
    $('select[name=place]').change(function(){
        select_change($(this).val());
    });
}

function select_change($val){
    console.log($val);
    $('.day').removeClass('occupied');
    $('.detail').hide();
    switch($val){
        case 'centros'  : 
            $('.day.estudio, .day.trabajo').addClass('occupied'); 
            $('.detail.estudio, .detail.trabajo').show(); 
        break; 
        case 'estudio'  :
            $('.day.estudio').addClass('occupied');
            $('.detail.estudio').show(); 
        break; 
        case 'trabajo'  :
            $('.day.trabajo').addClass('occupied');
            $('.detail.trabajo').show(); 
        break; 
        case 'taller'  :
            $('.day.taller').addClass('occupied');
            $('.detail.taller').show(); 
        break; 
        case 'multiuso'  :
            $('.day.multiuso').addClass('occupied');
            $('.detail.multiuso').show(); 
        break; 
        case 'visitantes'    :
            $('.day.visitantes').addClass('occupied');
            $('.detail.visitantes').show();
        break; 
        case 'todos'    :
            $('.day.estudio, .day.trabajo, .day.taller, .day.multiuso, .day.visitantes').addClass('occupied');
            $('.detail.estudio, .detail.trabajo, .detail.taller, .detail.multiuso, .detail.visitantes').show();
        break; 

    } 
}