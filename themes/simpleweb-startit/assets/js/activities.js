$(document).ready(function(){
	$('.activity h3, .activity h4, .activity .img-container').click(function(){
		openModal($(this).closest('.activity'));
	});
	$('.activity-modal .close-button').click(function(){
		$(this).parent().parent().hide();
	});
});
function openModal(activity){
	let title 			    = activity.find('.title').html();
	let date 			    = activity.find('.date').html();
	let imgUrl 			    = activity.find('.img-url').val();
	let description	        = activity.find('.description').val();
	let link 				= activity.find('.link').html();
	$('.activity-modal').find('h3').html(title);
	$('.activity-modal').find('img').attr('src',imgUrl);
	$('.activity-modal').find('p').html(description);
	$('.activity-modal').find('.date').html(date);
	$('.activity-modal').find('.link').html(link);
	$('.activity-modal').css('display','flex');
}
