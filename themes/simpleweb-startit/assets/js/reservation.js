$(document).ready(function(){
	initElements();
});

function initElements(){
    $('#mp_day').datepicker({ dateFormat: 'dd-mm-yy' })
	$('.centros input').click(function(){
        disableMultipurpose();
	});
	$('.multiuso input[type=radio]').click(function(){
        enableMultipurpose();
	});
    if ($('.multiuso input[type=radio]').prop('checked')){
        enableMultipurpose();
    }
    else{
        disableMultipurpose();
    }
    $('#solicitante').change(function(){
        $('.category_2 > div').hide();
        checkSolicitante();
    });
    checkSolicitante();
}

function disableMultipurpose(){
    $('.multiuso .fields textarea, .multiuso .fields input, .multiuso .fields select').removeAttr('name').removeAttr('required');
    $('.multiuso .fields').hide();
}

function enableMultipurpose(){
    $('.multiuso .fields').css('display','block');
    $('#mp_day').attr('name','mp_day');
    $('.multiuso .mp_from').attr('name','mp_from');
    $('.multiuso .mp_to').attr('name','mp_to');
    $('.multiuso .activity').attr('name','activity');
    $('.multiuso .people_number').attr('name','people_number');
    $('.multiuso .fields textarea, .multiuso .fields input, .multiuso .fields select').attr('required','required');
}

function checkSolicitante(){
    hideCategory2();
    switch ( $('#solicitante').val() ){
        case 'mensaje': 
            initMensaje();
        break;
        case 'organismo':
            initOrganismo();
        break;
    }
}
function hideCategory2(){
    $('#organism').removeAttr('required').removeAttr('name');
    $('#base_team').removeAttr('name');
}
function initMensaje(){
    $('.category_2 .mensaje').show();
    $('#comunity').attr('name','responsible_category_2');
}
function initOrganismo(){
    $('.category_2 .organismo').show();
    $('#organism').attr('required','required');
    $('#organism').attr('name','responsible_category_2');
    $('#base_team').attr('name','responsible_category_3');
}
